# Example of a user defined MPI datatype

To compile the example run `make all`. The resulting executable is `./build/bin/user-defined-datatype`.

If you would like to provide a specific MPI distribution use, set the `MPI` varialbe:
```bash
make MPI=<path to the MPI distribution installation directory> all
```

To run, execute:
```bash
mpiexc -n 2 -ppn 2 -hosts localhost ./build/bin/user-defined-datatype
```

The example uses two processes in the local host. The processes define an MPI data type, and then process 1 sends to process 0 a message with a single instance of the defined datatype.
