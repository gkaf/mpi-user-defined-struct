#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

/*
 * Explaining error reporting in an MPI call
 * 
 *  - Proceses are numbered from 0 to (N-1) where N is the number of processes.
 *  - Sending a message to process id -2 fails with an error message.
 *  - Sending a message to process with id -1 does not fail, because MPI_PROC_NULL = -1,
 *    so this sends a message to a special sink, the null process.
 */

int main(int argc, char** argv) {
	MPI_Init(NULL, NULL);
 
	int world_rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
	int world_size;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	
	// We are assuming at least 2 processes for this task
	if (world_size != 2) {
		fprintf(stderr, "World size must be 2 for %s\n", argv[0]);
		MPI_Abort(MPI_COMM_WORLD, 1);
	}
	
	int number;
	if (world_rank == 0) {
		number = -1;
		MPI_Send(&number, // data buffer
			1, // buffer size 
			MPI_INT, // data type
			-2, //destination
			0, //tag
			MPI_COMM_WORLD); // communicator
	} else if (world_rank == 1) {
		MPI_Recv(&number, 
			1, // buffer size
			MPI_INT, // data type
			0, // source
			0, //tag
			MPI_COMM_WORLD, // communicator
			MPI_STATUS_IGNORE);
	}
	
	MPI_Finalize();
}
