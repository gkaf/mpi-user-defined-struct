#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
#include "datatypes.h"

int main(int argc, char** argv) {
	MPI_Init(&argc, &argv);
	
	int count = 2;
	int array_of_blocklengths[] = {3,2};
	MPI_Aint* array_of_displacements = (MPI_Aint*) malloc(2*sizeof(MPI_Aint));
	MPI_Datatype array_of_types[] = {MPI_INT, MPI_DOUBLE};
	MPI_Datatype MPI_MYDATA;
	
	mydata t;
	MPI_Aint origin, address_ints, address_doubles;
	MPI_Get_address(&t, &origin);
	MPI_Get_address(&t.a, &address_ints);
	MPI_Get_address(&t.b, &address_doubles);
	array_of_displacements[0] = MPI_Aint_diff(address_ints, origin);
	array_of_displacements[1] = MPI_Aint_diff(address_doubles, origin);
	
	MPI_Type_create_struct(count, array_of_blocklengths, array_of_displacements, array_of_types, &MPI_MYDATA);
	MPI_Type_commit(&MPI_MYDATA);
	free(array_of_displacements);
	
	int world_rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
	if (world_rank == 1) {
		int tag = 0;
		
		t.a[0] = 0;
		t.a[1] = 1;
		t.a[2] = 2;
		t.b[0] = 0;
		t.b[1] = 1;
		
		MPI_Send(&t, 1, MPI_MYDATA, 0, tag, MPI_COMM_WORLD);
	} else if (world_rank == 0) {
		MPI_Status status;
		MPI_Recv(&t, 1, MPI_MYDATA, 1, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
		
		int data_count;
		MPI_Get_count(&status, MPI_MYDATA, &count);
		
		printf("Master recieved %d data structures:\n", count);
		printf("t.a[0] = %d\n", t.a[0]);
		printf("t.a[1] = %d\n", t.a[1]);
		printf("t.a[2] = %d\n", t.a[2]);
		printf("t.b[0] = %f\n", t.b[0]);
		printf("t.b[1] = %f\n", t.b[1]);
	}
	
	MPI_Type_free(&MPI_MYDATA);
	MPI_Finalize();
	
	return 0;
}
