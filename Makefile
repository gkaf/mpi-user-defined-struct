BUILD_DIR=build
OBJ_DIR=$(BUILD_DIR)/obj
BIN_DIR=$(BUILD_DIR)/bin

SRC_DIR=src
USER_IDIR=$(SRC_DIR)/include

$(info MPI demonstration programs)

c_flags=
ld_flags=

ifdef MPI
CC=clang
MPI_INSTALLATION=$(MPI)
IDIR=$(MPI_INSTALLATION)/include
LDIR=$(MPI_INSTALLATION)/lib

c_flags=-I$(IDIR)
ld_flags=-L$(LDIR) -Wl,-rpath=$(LDIR)
else
CC=mpicc
c_flags=-cc=clang
ld_flags=-cc=clang
endif

CFLAGS=$(c_flags) -I$(USER_IDIR)
LDFLAGS=$(ld_flags)
LIBS=-lmpi

_DEPS=datatypes.h
DEPS=$(patsubst %,$(USER_IDIR)/%,$(_DEPS))

.PHONY: all
all: user-defined-datatype send-err

.PHONY: user-defined-datatype
user-defined-datatype: prepare $(BIN_DIR)/user-defined-datatype

.PHONY: send-err
send-err: prepare $(BIN_DIR)/send-err

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

$(BIN_DIR)/user-defined-datatype: $(OBJ_DIR)/user-defined-datatype.o
	$(CC) -o $@ $^ $(LDFLAGS) $(LIBS)

$(BIN_DIR)/send-err: $(OBJ_DIR)/send-err.o
	$(CC) -o $@ $^ $(LDFLAGS) $(LIBS)

prepare: $(BUILD_DIR) $(OBJ_DIR) $(BIN_DIR)
	@{ if [ -z $(MPI) ]; then echo "Using system MPI distribution."; else echo "Using user provided MPI distribution: $(MPI)"; fi; }

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)
$(OBJ_DIR):
	mkdir -p $(OBJ_DIR)
$(BIN_DIR):
	mkdir -p $(BIN_DIR)
 
.PHONY: clean
clean:
	rm -rf $(BUILD_DIR) *~
